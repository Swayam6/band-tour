$mainBlue: #123456;
*{
    box-sizing: border-box;
}

body{
    margin: 0;
    padding: 0;
}

/*
::placeholder{
    color: #111;
}
*/

@keyframes name{
    from{
        opacity: 1;
        columns: 200px 2;
    }to{
        opacity: 0.1;
        columns: 50px 4;
    }
}

.box{
    color: $mainBlue;
    .box-item{
        box-sizing: border-box;
    }
}